The PTP plugin "SpinningImage" enables the rotating display of an image sequence.
This is free software, see the accompanying LICENSE file.
Version: 1.1
Has been tested with V2.5.3 of PTP on Linux.

Plugin description:
The plugin enables the rotating display of an image sequence.
The image file needs to be composed vertically, whith the individual frames lying upon each other.
You can select the action(s) that will be executed when clicking on the rotating image.The rotation, it's direction and speed can be set via PTP menus, the rotation and its behaviour can NOT be controled via mouse events or gestures.

Plugin usage:
The plugin can be started and stopped via the PTP actions selector, so you may assign the start to a button or hotspot,
and the termination of the rotation and removal of the image to the spinning image itself.
The plugin can be incorporated multiple times into a tour, each instance having its own image and display settings.
